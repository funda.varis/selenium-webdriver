package pages;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;


public class HomePage extends BasePage {

    public final By womanButiQue = By.cssSelector("a[href$='kadin']");
    public final By firstCampaingnAd = By.cssSelector(".sticky-wrapper .component-item:nth-child(1)");
    Integer invalidImageCount;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void clickWomanButique() {
        waitVisibility(womanButiQue);
        click(womanButiQue);
    }

    public Integer verifyImageActive(WebElement imgElement) {
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(imgElement.getAttribute("src"));
            HttpResponse response = client.execute(request);
            // verifying response code he HttpStatus should be 200 if not,
            // increment as invalid images count
            if (response.getStatusLine().getStatusCode() != 200)
                invalidImageCount++;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return invalidImageCount;
    }

    public void validateInvalidImages() {
        try {
            invalidImageCount = 0;
            List<WebElement> imagesList = driver.findElements(By.cssSelector(".sticky-wrapper .component-item"));
            logger.info("Total no. of images are {}: ", imagesList.size());
            for (WebElement imgElement : imagesList) {
                if (imgElement != null) {
                    verifyImageActive(imgElement);
                }
            }
            logger.info("Total no. of invalid images are: {}.", invalidImageCount);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    public void goToFirstCampaign() {
        waitUntilElementPresented(firstCampaingnAd);
        click(firstCampaingnAd);
    }


}
