package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.DataOperations;

import java.io.FileNotFoundException;

public class LoginPage extends BasePage {

    private By usernameField = By.id("login-email");
    private By passwordField = By.id("login-password-input");
    private By loginButton = By.xpath("//*[@type='submit']//span[text()='Giriş Yap']");
    public final By accountTittle = By.xpath("//*[@class='link-text' and text()='Hesabım']");
    public final By genderSelectorClose = By.xpath("//*[@id='Combined-Shape']");
    private final By loginIcon = By.xpath("//*[@class='link-text' and text()='Giriş Yap']");
    protected DataOperations dataOperations = new DataOperations();

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public synchronized void shouldCloseGenderSelector() {
        click(genderSelectorClose);
    }

    public void verifyAccountTitle() {
        waitUntilElementPresented(accountTittle);
    }

    public void setUserName(String userName) {
        sendKeys(usernameField, userName);
    }


    public void setPassword(String password) {
        sendKeys(passwordField, password);
    }

    public void clickLoginButton() {
        click(loginButton);
    }

    public void clickLogin() {
        click(loginIcon);
    }

    public void loginTest() throws FileNotFoundException {
        driver.get(dataOperations.readJsonFile("BASEURL"));
        driver.manage().window().maximize();
        shouldCloseGenderSelector();
        logger.debug(driver.getTitle());
        clickLogin();
        setUserName(dataOperations.readJsonFile("USERNAME"));
        setPassword(dataOperations.readJsonFile("PASSWORD"));
        clickLoginButton();
        verifyAccountTitle();
    }


}
