package utilities;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class DataOperations {

    JsonParser parser;
    static final String FILEPATH = "src/test/resources/data.json";
    Object object;
    String value;

    public String readJsonFile(String dataValue) throws FileNotFoundException {
        parser = new JsonParser();
        object = parser.parse(new FileReader(FILEPATH));
        JsonObject jsonObject = (JsonObject) object;
        value = jsonObject.get(dataValue).getAsString();
        return value;

    }

}
