package utilities;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;


public class WebDriverOperations {

    private static final Logger logger = LogManager.getLogger(WebDriverOperations.class);
    DataOperations dataOperations = new DataOperations();
    ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    public synchronized ThreadLocal<WebDriver> getWebDriver(String browser, String type) throws FileNotFoundException, MalformedURLException {
        if (type.equals("local")) {
            switch (browser.toLowerCase()) {
                case "firefox" -> {
                    WebDriverManager.firefoxdriver().setup();
                    driver.set(new FirefoxDriver());
                }
                case "chrome" -> {
                    WebDriverManager.chromedriver().setup();
                    driver.set(new ChromeDriver());
                }
                case "edge" -> {
                    WebDriverManager.edgedriver().setup();
                    driver.set(new EdgeDriver());
                }
                default -> logger.warn("Browser type is not configured!");
            }
        } else if (type.equals("remote")) {
            driver.set((getRemoteWebDriver(browser).get()));
        } else {
            logger.warn("Type is invalid. Choose 'local' or 'remote'.");
        }

        return driver;
    }

    public synchronized ThreadLocal<RemoteWebDriver> getRemoteWebDriver(String browser) throws FileNotFoundException, MalformedURLException {
        ThreadLocal<RemoteWebDriver> threadLocalDriver = new ThreadLocal<>();
        URL hubURL = new URL(dataOperations.readJsonFile("HUB_URL"));
        String platformName = dataOperations.readJsonFile("PLATFORM");
        switch (browser.toLowerCase()) {
            case "firefox" -> {
                FirefoxOptions options = new FirefoxOptions();
                options.addArguments("PlatformName", platformName);
                options.addArguments("--start-maximized");
                RemoteWebDriver remoteDriver = new RemoteWebDriver(hubURL, options);
                threadLocalDriver.set(remoteDriver);

            }
            case "chrome" -> {
                ChromeOptions options = new ChromeOptions();
                options.addArguments("PlatformName", platformName);
                options.addArguments("--no-sandbox");// Bypass OS security model
                options.addArguments("--ignore-certificate-errors");
                options.addArguments("--start-maximized");
                options.addArguments("--disable-popup-blocking");
                RemoteWebDriver remoteDriver = new RemoteWebDriver(hubURL, options);
                threadLocalDriver.set(remoteDriver);

            }
            case "edge" -> {
                EdgeOptions options = new EdgeOptions();
                options.addArguments("PlatformName", platformName);
                RemoteWebDriver remoteDriver = new RemoteWebDriver(hubURL, options);
                threadLocalDriver.set(remoteDriver);

            }
            default -> logger.warn("Remote browser type is not configured!");

        }
        return threadLocalDriver;

    }

    public synchronized WebDriver getDriver() {
        return driver.get();
    }
}