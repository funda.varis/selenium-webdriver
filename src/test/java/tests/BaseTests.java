package tests;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.HomePage;
import pages.LoginPage;
import utilities.DataOperations;
import utilities.WebDriverOperations;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;

public class BaseTests {
    protected ThreadLocal<WebDriver> driver = new ThreadLocal<>();
    protected DataOperations dataOperations = new DataOperations();
  protected HomePage homePage;
  protected LoginPage loginPage;
    WebDriverOperations webDriverOperations = new WebDriverOperations();
    public static final Logger logger = LogManager.getLogger(BaseTests.class);

    @BeforeMethod
    public void setup() throws FileNotFoundException, MalformedURLException {
        driver = webDriverOperations.getWebDriver(dataOperations.readJsonFile("BROWSER"),
                dataOperations.readJsonFile("ENV_TYPE"));
        loginPage= new LoginPage(driver.get());
        homePage= new HomePage(driver.get());
    }

    @AfterMethod
    public void closeBrowser() {
        webDriverOperations.getDriver().quit();
    }

}
