package tests;

import org.testng.annotations.Test;

import java.io.FileNotFoundException;

public class HomePageTests extends BaseTests {

    @Test
    public void checkWomanViewLoaded() throws FileNotFoundException {
        loginPage.loginTest();
        homePage.clickWomanButique();
        homePage.validateInvalidImages();
    }

    @Test
    public void goToRandomCampaign() throws FileNotFoundException {
        loginPage.loginTest();
        homePage.clickWomanButique();
        homePage.goToFirstCampaign();
    }

}